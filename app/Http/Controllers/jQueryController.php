<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class jQueryController extends Controller
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the jQuery playground
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('jquery.index');
    }
}
