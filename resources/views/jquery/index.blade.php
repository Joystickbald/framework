@extends('layouts.jQapp')

@section('content')
<div id = "jQcontent"> 

    <header>
        <h1> Gwent Memory Test</h1>
        <p>Inspired by <a href = "https://www.playgwent.com/pl" target = "_blank">Gwent - The Witcher Card Game</a></p>
    </header>

    <main>
            <article>
                <div class = "board">
                    <div class = "card" id = "0" data-index-number = "0"></div>
                    <div class = "card" id = "1" data-index-number = "1"></div>
                    <div class = "card" id = "2" data-index-number = "2"></div>
                    <div class = "card" id = "3" data-index-number = "3"></div>

                    <div class = "card" id = "4" data-index-number = "4"></div>
                    <div class = "card" id = "5" data-index-number = "5"></div>
                    <div class = "card" id = "6" data-index-number = "6"></div>
                    <div class = "card" id = "7" data-index-number = "7"></div>
                    
                    <div class = "card" id = "8" data-index-number = "8"></div>
                    <div class = "card" id = "9" data-index-number = "9"></div>
                    <div class = "card" id = "10" data-index-number = "10"></div>
                    <div class = "card" id = "11" data-index-number = "11"></div>

                    <div class = "score">Turn counter: 0</div>
                </div>
            </article>
    </main>


</div>

<script src ="/storage/jquery-3.3.1.min.js"></script>

<script>
 $(document).ready(game());

function game()
{
    // var cards = ["ciri.png", "geralt.png", "jaskier.png", "jaskier.png", "iorweth.png", "triss.png", "geralt.png", "yen.png", "ciri.png", "triss.png", "yen.png", "iorweth.png"];
    
    // console.log(cards);
    var cards = [];
    var cardDeck = ["ciri.png", "geralt.png", "jaskier.png", "iorweth.png", "triss.png", "yen.png"];

    function setCards()
    {
        var i = 1;
        var number;
        var pairsSet = false;

        while ( i < 6)
        {
            number = Math.floor(Math.random() * 12);
            console.log(number);
            cards[number] = cardDeck[i];
            if (pairsSet == true)
            {
                i++;
                pairsSet = false;
            }
            else
            {
                pairsSet = true;
            }
        }
        console.log(cards);
    }

    setCards();
    $('.card').click(function()
        { 
            revealCard(this.dataset.indexNumber);
        }
    ); 
    
    var oneVisible = false;
    var turnCounter = 0;
    var visible_nr;
    var lock = false;
    var pairsLeft = 6;

    function revealCard(nr)
    {
        var opacityValue = $('#' + nr).css('opacity');
        
        if (opacityValue != 0 && lock == false)
        {
            lock = true;
        
            var obraz = "url(/storage/img/" + cards[nr] + ")";
            
            $('#'+ nr).css('background-image', obraz);
            $('#'+ nr).addClass('cardActive');
            $('#'+ nr).removeClass('card');
            
            if(oneVisible == false)
            {
                //first card
                oneVisible = true;
                visible_nr = nr;
                lock = false;
            }
            else
            {
                //second card
                if(cards[visible_nr] == cards[nr])
                {
                    setTimeout(function() { hide2Cards(nr, visible_nr) }, 750);
                }
                else
                {
                    setTimeout(function() { restore2Cards(nr, visible_nr) }, 1000);
                }
                
                turnCounter++;
                $('.score').html('Turn counter: '+turnCounter);
                oneVisible = false;
            }   
        }
    }

    function hide2Cards(nr, visible_nr)
    {
        $('#' + nr).css('opacity', '0');
        $('#' + visible_nr).css('opacity', '0');
        
        pairsLeft--;
        
        if(pairsLeft == 0)
        {
            $('.board').html('<h1>You win!<br>Done in ' + turnCounter + ' turns</h1><br> <span id = "restart">Play again?</span>');
        }
        
        lock = false;
    }

    function restore2Cards(nr, visible_nr)
    {
        $('#' + nr).css('background-image', 'url(/storage/img/karta.png)');
        $('#' + nr).addClass('card');
        $('#' + nr).removeClass('cardActive');	

        $('#' + visible_nr).css('background-image', 'url(/storage/img/karta.png)');
        $('#' + visible_nr).addClass('card');
        $('#' + visible_nr).removeClass('cardActive');
        
        lock = false;
    }
}

</script>
@endsection