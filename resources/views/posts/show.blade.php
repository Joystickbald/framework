@extends('layouts.app')

@section('content')
    <a href = "/posts" class = "btn btn-default">Smeagoul Go Back!</a>

    <h1>{{$post->title}}</h1>
    
        <img style = "width:150px" src = "/storage/cover_images/{{$post->cover_image}}">
    <hr>
    <div>
        <p>{{$post->body}}</p>
    </div>

    <hr>

    <small>Napisane dnia {{$post->created_at}} przez {{$post->user->name}}</small>

    <hr>
    @if(!Auth::guest())
        @if(Auth::user()->id == $post->user_id)
            <a href = "/posts/{{$post->id}}/edit" class = "btn btn-default">Edit</a>

            {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right']) !!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
    @endif
@endsection