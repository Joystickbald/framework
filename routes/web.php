<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/controller', 'PagesController@index');
Route::get('/google', 'PagesController@google');
Route::get('/strona', 'PagesController@strona');
Route::get('/', 'PagesController@strona');
Route::get('/jQuery','jQueryController@index');

Route::resource('posts','PostsController');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');
